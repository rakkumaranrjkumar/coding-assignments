import java.util.*;
import java.util.Arrays;
import java.util.ArrayList;  
public class Daytwo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Name getting from user input
		Scanner name = new Scanner(System.in);			
		System.out.println("Name:");
		String str = name.nextLine();
		System.out.println(str);
		System.out.println();
		
		//Task Updation
		String[] Datafeeding = {"Getting Data", "Data Insertion", "Data Updation", "Deleting Repitition", "Finalizing Data" };
		System.out.println("Tasks are listed below");
		for(int i=0;i<(Datafeeding.length);i++)
		{
			System.out.println(Datafeeding[i]);
		}
		System.out.println();
		String add = "Data Interpretation";
		List<String> a = new ArrayList<String>(Arrays.asList(Datafeeding)); // Convertion of Array to ArrayList
		a.add(add);
	    Datafeeding = a.toArray(Datafeeding); // Convertion of ArrayList to Array
		System.out.println("After Updating:" + Arrays.toString(Datafeeding));
	// Task Deletion
		System.out.println("Element to be deleted:");
		String delete = name.nextLine();
		for(int i = 0; i < Datafeeding.length; i++)
	    {
	      if(Datafeeding[i] == delete)   
	      {
	        for(int j = i; j < Datafeeding.length-1; j++)
	        {
	        	Datafeeding[j] = Datafeeding[j+1];
	        }
	        break;
	      }
	    }
		System.out.println("Elements after deletion: " );
	    for(int i = 0; i < Datafeeding.length-1; i++)
	    {
	    	System.out.print(Datafeeding[i] + " ");
	    }
	    // Task Searching
	    System.out.println();
	    System.out.println("Searching the data:");
	    int index=0;
	    boolean x = false;   
        String search = "Data Updation";
        for (int i = 0; i < Datafeeding.length; i++) 
        {  
            if(search.equals(Datafeeding[i])) 
            { 
            	index = i;
                x = true; 
                break;  
            }  
        }  
	    if(x)
	    {
			System.out.println(search + " Found at index "+ index);
	    }
	    else
	    {
	    	System.out.println("Not Found");
	    }
	}
}